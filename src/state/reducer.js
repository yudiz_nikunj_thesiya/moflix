export const initialState = {
	likedMovies: [],
};

function reducer(state, action) {
	switch (action.type) {
		case "ADD_TO_LIKED":
			return {
				...state,
				likedMovies: [...state.likedMovies, { ...action.item }],
			};
		case "REMOVE_FROM_LIKED":
			let newlikedMovies = [...state.likedMovies];
			const index = state.likedMovies.findIndex(
				(Item) => Item.id === action.id
			);

			if (index >= 0) {
				newlikedMovies.splice(index, 1);
			}
			return { ...state, likedMovies: newlikedMovies };

		default:
			return state;
	}
}

export default reducer;
