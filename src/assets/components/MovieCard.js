import React, { useEffect, useState } from "react";
import "../styles/moviecard.scss";
import { BsStarFill } from "react-icons/bs";
import { RiHeartLine, RiHeartFill } from "react-icons/ri";
import { useStateValue } from "../../state/StateProvider";
import Clamp from "react-multiline-clamp";

const MovieCard = ({ id, img, label, heading, desc, rating, liked }) => {
	const [likedBtn, setLikedBtn] = useState(false);
	const [{ likedMovies }, dispatch] = useStateValue();
	useEffect(() => {
		likedMovies.map((item) => item.id === id && setLikedBtn(true));
	}, likedMovies);
	const addToLiked = () => {
		dispatch({
			type: "ADD_TO_LIKED",
			item: {
				id: id,
				img: img,
				label: label,
				heading: heading,
				desc: desc,
				rating: rating,
				liked: true,
			},
		});
		setLikedBtn(true);
	};
	const removeFromLiked = () => {
		dispatch({
			type: "REMOVE_FROM_LIKED",
			id: id,
		});
		setLikedBtn(false);
	};
	return (
		<div className="card">
			<img
				src={`https://image.tmdb.org/t/p/w500${img}`}
				alt="house"
				className="card__image"
			/>
			{likedBtn ? (
				<div className="card__like" onClick={() => removeFromLiked()}>
					<RiHeartFill />
				</div>
			) : (
				<div className="card__like" onClick={() => addToLiked()}>
					<RiHeartLine />
				</div>
			)}
			<div className="card__details">
				<span className="card__details--label">{label}</span>
				{rating > 9 ? (
					<div className="card__details--star">
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
					</div>
				) : rating > 7 ? (
					<div className="card__details--star">
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
					</div>
				) : rating > 5 ? (
					<div className="card__details--star">
						<BsStarFill />
						<BsStarFill />
						<BsStarFill />
					</div>
				) : rating > 3 ? (
					<div className="card__details--star">
						<BsStarFill />
						<BsStarFill />
					</div>
				) : (
					<div className="card__details--star">
						<BsStarFill />
					</div>
				)}

				<Clamp withTooltip lines={1}>
					<span className="card__details--heading">{heading}</span>
				</Clamp>
				<Clamp withTooltip lines={2}>
					<span className="card__details--desc">{desc}</span>
				</Clamp>
			</div>
		</div>
	);
};

export default MovieCard;
