import React from "react";
import { AiOutlineHeart } from "react-icons/ai";
import { Link } from "react-router-dom";
import logo from "../images/whitelogo.png";
import "../styles/nav.scss";
import { useStateValue } from "../../state/StateProvider";

const Nav = () => {
	const [{ likedMovies }, dispatch] = useStateValue();
	return (
		<div className="nav">
			<Link to="/" className="nav__logo">
				<img src={logo} alt="logo" />
			</Link>

			<div className="nav__menu">
				<Link to="/likedmovies" className="nav__menu--cart">
					<AiOutlineHeart className="nav__menu--cart-icon" />
					<span className="nav__menu--cart-label">{likedMovies?.length}</span>
				</Link>
			</div>
		</div>
	);
};

export default Nav;
