import { useEffect, useState } from "react";

const useData = () => {
	const [movies, setMovies] = useState([]);
	const API_URL =
		"https://api.themoviedb.org/3/movie/popular?api_key=ce213a59c85f3bbbc89389cb5c67d3a6";
	useEffect(() => {
		const getData = async () => {
			await fetch(API_URL)
				.then((res) => res.json())
				.then((data) => {
					setMovies(data.results);
				});
		};
		getData();
	}, []);
	return movies;
};

export default useData;
