import React from "react";
import { useStateValue } from "../../state/StateProvider";
import MovieCard from "../components/MovieCard";
// import CartProduct from "../assets/components/CartProduct";
import "../styles/liked.scss";

const Liked = () => {
	const [{ likedMovies }, dispatch] = useStateValue();

	return (
		<div className="liked">
			<div className="liked__header">
				<span className="liked__header-heading">
					You Liked {likedMovies?.length} Movies.
				</span>
			</div>
			<div className="liked__items">
				{likedMovies?.map((movie) => (
					<MovieCard
						id={movie.id}
						key={movie.id}
						img={movie.img}
						label={movie.label}
						heading={movie.heading}
						desc={movie.desc}
						rating={movie.rating}
					/>
				))}
			</div>
		</div>
	);
};

export default Liked;
