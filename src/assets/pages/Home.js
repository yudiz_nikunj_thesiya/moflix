import React, { useEffect, useState, useRef } from "react";
import { FiSearch } from "react-icons/fi";

import MovieCard from "../components/MovieCard";
import useData from "../hooks/useData";
import "../styles/herosection.scss";
import "../styles/home.scss";

const Home = () => {
	const [searchTerm, setSearchTerm] = useState("");
	// const searchRef = useRef(null);
	const movies = useData();

	// const handleClick = (e) => {
	// 	e.preventDefault();
	// 	setSearchTerm(searchRef.current.value);
	// };

	return (
		<div>
			<div className="banner">
				<span className="banner__title">Search Your Favorite Movie.</span>
				<form
					className="banner__search"
					onSubmit={(e) => {
						e.preventDefault();
					}}
				>
					<input
						type="text"
						className="banner__search--input"
						placeholder="Search Movie"
						value={searchTerm}
						onChange={(e) => {
							setSearchTerm(e.target.value);
						}}
						// ref={searchRef}
					/>
					<button
						type="submit"
						className="banner__search--btn"
						// onClick={handleClick}
					>
						<FiSearch />
					</button>
				</form>
			</div>
			<div className="card-container">
				{movies
					.filter((item) => {
						if (item.title.toLowerCase().includes(searchTerm.toLowerCase())) {
							return item;
						}
					})
					.map((movie) => (
						<MovieCard
							id={movie.id}
							key={movie.id}
							img={movie.poster_path}
							label={movie.vote_average}
							heading={movie.title}
							desc={movie.overview}
							rating={movie.vote_average}
							liked=""
						/>
					))}
			</div>
		</div>
	);
};

export default Home;
