import React from "react";
import Nav from "./assets/components/Nav";
import { Routes, Route } from "react-router-dom";
import Home from "./assets/pages/Home";
import Liked from "./assets/pages/Liked";

function App() {
	return (
		<div className="App">
			<Nav />
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/likedmovies" element={<Liked />} />
			</Routes>
		</div>
	);
}

export default App;

// ce213a59c85f3bbbc89389cb5c67d3a6
// https://api.themoviedb.org/3/movie/550?api_key=ce213a59c85f3bbbc89389cb5c67d3a6
// eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjZTIxM2E1OWM4NWYzYmJiYzg5Mzg5Y2I1YzY3ZDNhNiIsInN1YiI6IjYyMjA3OWE3NzcxOWQ3MDA2ZGE1ZGYzMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.ZvSh6TFVtc4qgPj6xqZeAW9UB0F62DGvyusqvQ2Y7ow
